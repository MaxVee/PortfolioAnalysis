# Крипто-инвестиции(last 02.07.17)



## Общая информация

### Это форумы на которых анонсы и обсуждение криптовалют:
[Анонс криптовалют](https://cryptocurrencytalk.com/forum/700-cryptocurrency-announcements/)

### Эти приложения для мониторинга курсов, рачсета индексов и портфелей криптовалют на рынках:
[Мониторинг рынков CryptoCompare, есть API для выкачивания данных](https://www.cryptocompare.com/)

[Здесь можно бесплатно зарегестрироваться и мониторить рынки](https://cointracking.info/)

[Агрегатор информации с бирж, удобно смотреть](http://coinmarketcap.com/currencies/views/all/)

[Монитор Bitcoin](https://blockchain.info/)

### Блоги, новости
[Blog Satoshi Fund, это блог с анонсами ICO от инвест фонда](https://blog.cyber.fund/)
> Посмотри их "пирог"/портфель криптовалют (https://satoshi.fund). Марина Гурьева из команды выступала в Вышке. 
> Они 5% за ввод и 5% за вывод берут. У них готовый портфель. 
> Не надо заморачиваться с отдельными кошельками для каждой валюты.   



## Калькуляция прибыльности майнинга и хэшрэйт
[Хэшрэйт для rx-580](https://forum.ethereum.org/discussion/11973/rx-580-hashrate)
> Это карта для фермы из объявления. Хэшрэт ~25-28 mh/s. Ферма из 6 карт ~6*25=150 mh/s ~ 700$

[Kалькулятор хэшрэйт для эфириума](http://ethereum-mining-calculator.com)

[Агрегированный калькулятор прибольности и параметров майнинга для разных валют](https://whattomine.com/) 

[Агрегированный калькулятор и монитор](https://www.coinwarz.com/calculators)


## Интересные монеты
[Zcash](https://z.cash/)
> Можно майнить

> на карте NVIDIA GTX 1070  ~420 h/s ~3.5-3.9$ в день

> [расчет в статье](http://cryptomining-blog.com/tag/gtx-1070-hashrate/)

> [расчет в калькуляторе](http://whattomine.com/coins?utf8=%E2%9C%93&eth=true&factor[eth_hr]=28.5&factor[eth_p]=0.0&grof=true&factor[gro_hr]=35.5&factor[gro_p]=0&x11gf=true&factor[x11g_hr]=11.5&factor[x11g_p]=0&cn=true&factor[cn_hr]=500.0&factor[cn_p]=0&eq=true&factor[eq_hr]=420.0&factor[eq_p]=0&lre=true&factor[lrev2_hr]=35500.0&factor[lrev2_p]=0&ns=true&factor[ns_hr]=1050.0&factor[ns_p]=0&lbry=true&factor[lbry_hr]=275.0&factor[lbry_p]=0&bk2bf=true&factor[bk2b_hr]=1600.0&factor[bk2b_p]=0.0&bk14=true&factor[bk14_hr]=2500.0&factor[bk14_p]=0.0&pas=true&factor[pas_hr]=940.0&factor[pas_p]=120.0&bkv=true&factor[bkv_hr]=0.0&factor[bkv_p]=0.0&factor[cost]=0.1&sort=Profitability24&volume=100&revenue=24h&factor[exchanges][]=&factor[exchanges][]=bittrex&factor[exchanges][]=bleutrade&factor[exchanges][]=btc_e&factor[exchanges][]=bter&factor[exchanges][]=c_cex&factor[exchanges][]=cryptopia&factor[exchanges][]=poloniex&factor[exchanges][]=yobit&dataset=Main&commit=Calculate&adapt_q_280x=0&adapt_q_380=0&adapt_q_fury=0&adapt_q_470=0&adapt_q_480=0&adapt_q_750Ti=0&adapt_q_10606=0&adapt_q_1070=1&adapt_q_1080=0)

> оказывается самой прибыльной перед остальными из расчета майнинга эфириума 

[Decred](https://www.decred.org/)
> На ферме из объявления майнит 

[Monero](https://getmonero.org/)
> Можно майнить

[Storj - интересный проект](https://storj.io/index.html)

## Интересные проекты
[Humaniq Bank](https://humaniq.co/)

[Голос](http://golosd.com/)

[Распределенные вычисления Golem](https://www.smithandcrown.com/golem-will-enable-p2p-computing-market/)

[Monaco VISA® Card](https://mona.co/#section-2)

[web3j is a lightweight, reactive, type safe Java and Android library for integrating with nodes on Ethereum blockchains](https://web3j.io/)

[От создателя web3j, building the enterprise blockchain](https://blk.io/)



## Ивестиции в фонды
https://satoshi.fund

https://thetoken.io


## Ивестиции в биржи

## Ивестиции в майнинг

## Инвестиции в ICO :tomato:
:exclamation: [TokenMarket is a marketplace for tokens, digital assets and blockchain based investing](https://tokenmarket.net)

[Список с анонсами ICO](https://www.smithandcrown.com/)

[Список ICO](http://www.the-blockchain.com/ico-list-com/)

:tomato: [BankEx](https://bankex.org/)
> Предлагают ликвидность для "слаболиквидных" активов. Сервис для банков и финансовых компаний. 
> :hotdog: Для зарегестрированных инвесторов есть пресейл. 
> Скоро ICO.

[Биржа труда](https://www.blocklancer.net/)
> Скоро ICO. :lollipop: Есть баунти.

:tomato: [Страхование](https://www.insurex.co/)
> Скоро ICO. 

https://www.bitquence.com/tokensale/
> Идет ICO

:tomato: [Проект по созданию регулируемого банка для цифрового поколения](https://polybius.io)
> Идет ICO

:tomato: [Tokenizing mobile data industry](https://tokenmarket.net/blockchain/ethereum/assets/dent/)
> Скоро ICO.  

## Технологии
[ethOS-операционная система для майнинга. Можно майнить Ethereum, Zcash, Monero и автотрейдить их в биткоины](http://ethosdistro.com)